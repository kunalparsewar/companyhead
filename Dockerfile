FROM node:8

#create appdirecctory
WORKDIR /usr/app/src

#install app dependencies

COPY package*.json ./

RUN npm install 

COPY . .
EXPOSE 8080
CMD [ "npm" , "start" ]

